"""
entrypoint to bib ocr
"""
import asyncio
import logging
import pickle

import websockets
import websockets.server

import inference


async def handler(websocket):
    """
    handle requests for bib identification
    :param websocket:
    """
    async for message in websocket:
        inference_result = inference.identify(pickle.loads(message))
        logging.debug(
            f"Found bib: {str(inference_result[1] if inference_result is not None else None)}")
        await websocket.send(pickle.dumps(inference_result))


async def main():
    """
    start a forever running server
    """
    port = 8004
    logger = logging.getLogger()
    async with websockets.serve(handler, "", port, logger=logger, max_size=2 ** 22):
        await asyncio.Future()  # run forever


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    asyncio.run(main())
