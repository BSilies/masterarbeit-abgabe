"""
module doing inference using easyOCR
"""
import re
import traceback

import easyocr
import numpy as _np

_reader = easyocr.Reader(['de'])
_pattern = re.compile(r"[0-9fgiloqstzABDGHIJOQSTZ]*\d[0-9fgiloqstzABDGHIJOQSTZ]*")


def identify(image: _np.array):
    """
    run the inference
    :param image:
    :return:
    """
    try:
        result = _reader.readtext(image)
        result_filtered = list(filter(lambda z: re.fullmatch(_pattern, z[1]), result))
        if len(result_filtered) > 0:
            result_filtered.sort(key=lambda x: x[2], reverse=True)
            return result_filtered[0]
        return None
    except:
        traceback.print_exc()
        return None
