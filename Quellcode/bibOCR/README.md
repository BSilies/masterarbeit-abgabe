# OCR-Modul zur Identifikation

## Installation

Es wird Python Version 3.10 und pip benötigt.

Zusätzlich
muss [CUDA 11.7](https://developer.nvidia.com/cuda-11-7-0-download-archive?target_os=Windows&target_arch=x86_64&target_version=11&target_type=exe_local)
mit [CUDNN](https://docs.nvidia.com/deeplearning/cudnn/install-guide/index.html) installiert werden. Eine NVIDIA-Grafikkarte mit CUDA-Unterstützung ist notwendig.

Je nach Betriebssystems das Script ``setup.bat`` oder ``setup.sh`` ausführen. In der
Datei ``setup.bat`` muss der Dateipfad zur Python-Installation angegeben werden.

## Ausführung

Der Server kann nach der Installation in der virtuellen Umgebung mit dem
Befehl ``python app.py`` gestartet werden.

Der Server beendet sich nicht von selbst, sondern muss aktiv beendet werden.
