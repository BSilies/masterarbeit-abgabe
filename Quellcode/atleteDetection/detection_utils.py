"""
some utils for detection
"""
import math

TOLERANCE_DISTANCE = 0.1

RELEVANT_DETECTION_DISTANCE = 2

WIDTH = 848
HEIGHT = 480

NUMBER_OF_CELLS = 106  # 53, 106, 212
CELL_HEIGHT_FACTOR = 2
ACTIVATION_THRESHOLD = 2 / 3


def calculate_ratio(ref_point, height, distance_to_earth):
    """
    calculate the ratio between the relative distances between the ref point and the edge of the
    image on ground-level and detection-level
    :param ref_point:
    :param height:
    :param distance_to_earth:
    :return:
    """
    width_view_half = math.tan(43 * 2 * math.pi / 360) * height
    width_view_earth_half = math.tan(43 * 2 * math.pi / 360) * distance_to_earth / 100
    return abs(WIDTH / 2 - ref_point) / (WIDTH / 2) * width_view_earth_half / width_view_half
