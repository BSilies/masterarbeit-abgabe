"""
the detector
"""
import datetime
from typing import Tuple, List

import numpy as np

import detection_utils as utils


class Detector:
    """
    class for detecting people crossing a line
    """
    _TOLERANCE_DISTANCE = 0.1
    _RELEVANT_DETECTION_DISTANCE = 2
    _DISTANCE_TO_EARTH = 291.7
    _FINISH_LINE = ((0, 260), (utils.WIDTH, 215))
    _REF_POINTS = (((0, (319, 79)), (1.488, (215, 77))), ((0, (521, 68)), (1.488, (620, 63))))
    _depth_scale = 0

    _SENSOR = []
    _finish_line_redefined: Tuple[Tuple[int, int], Tuple[int, int]]

    image_y_min: int
    image_y_max: int

    _max_image: np.ndarray
    _max_image_tolerance: float
    _dead_earth_space: float
    _dead_sky_space: float
    _images_array: List[np.ndarray] = []
    _previous_detections: List[Tuple[int, int]] = []
    _detections: List[Tuple[int, int]] = []
    _active_cells: List[Tuple[int, float]] = []

    _depth_image: np.ndarray
    _binary_image: np.ndarray

    _current_gap = 0

    def __init__(self, depth_scale: float) -> None:
        self._max_image_tolerance = self._TOLERANCE_DISTANCE / depth_scale
        self._dead_earth_space = self._RELEVANT_DETECTION_DISTANCE / depth_scale
        self._dead_sky_space = 0.4 / depth_scale
        self._depth_scale = depth_scale
        self._redefine_finish_line()
        self._define_crop()
        self._build_sensor()

    def _redefine_finish_line(self) -> None:
        """
        redefine the finish line coordinates for the cropped image
        """
        self._finish_line_redefined = (
            (0,
             40 + int(
                 utils.WIDTH
                 / utils.NUMBER_OF_CELLS * utils.CELL_HEIGHT_FACTOR * utils.ACTIVATION_THRESHOLD)
             + max(0, (self._FINISH_LINE[0][1] - self._FINISH_LINE[1][1]))),
            (utils.WIDTH,
             40 + int(
                 utils.WIDTH
                 / utils.NUMBER_OF_CELLS * utils.CELL_HEIGHT_FACTOR * utils.ACTIVATION_THRESHOLD)
             + max(0, (self._FINISH_LINE[1][1] - self._FINISH_LINE[0][1]))))

    def _define_crop(self) -> None:
        """
        crop the image around the finish line
        """
        self.image_y_min = min(self._FINISH_LINE[0][1], self._FINISH_LINE[1][1]) - int(
            utils.WIDTH
            / utils.NUMBER_OF_CELLS * utils.CELL_HEIGHT_FACTOR * utils.ACTIVATION_THRESHOLD) - 40
        self.image_y_max = max(self._FINISH_LINE[0][1], self._FINISH_LINE[1][1]) + int(
            utils.WIDTH / utils.NUMBER_OF_CELLS * utils.CELL_HEIGHT_FACTOR * (
                    1 - utils.ACTIVATION_THRESHOLD)) + 40
        self._max_image = np.zeros((self.image_y_max - self.image_y_min, utils.WIDTH), dtype=int)

    def _build_sensor(self) -> None:
        """
        build the sensor for detection
        """
        for cell_index in range(0, utils.NUMBER_OF_CELLS):
            self._SENSOR.append(
                [[int(cell_index * utils.WIDTH / utils.NUMBER_OF_CELLS),
                  int(self._finish_line_redefined[0][1]
                      + (self._finish_line_redefined[1][1]
                         - self._finish_line_redefined[0][1])
                      / utils.NUMBER_OF_CELLS / 2
                      + cell_index * (
                              self._finish_line_redefined[1][1]
                              - self._finish_line_redefined[0][1])
                      / utils.NUMBER_OF_CELLS
                      - utils.WIDTH
                      / utils.NUMBER_OF_CELLS * utils.CELL_HEIGHT_FACTOR * utils.ACTIVATION_THRESHOLD)
                  ],
                 [int((cell_index + 1) * utils.WIDTH / utils.NUMBER_OF_CELLS),
                  int(self._finish_line_redefined[0][1]
                      + ((self._finish_line_redefined[1][1]
                          - self._finish_line_redefined[0][1])
                         / utils.NUMBER_OF_CELLS) / 2
                      + cell_index * (
                              self._finish_line_redefined[1][1]
                              - self._finish_line_redefined[0][1])
                      / utils.NUMBER_OF_CELLS
                      + utils.WIDTH / utils.NUMBER_OF_CELLS * utils.CELL_HEIGHT_FACTOR * (
                              1 - utils.ACTIVATION_THRESHOLD))]])

    def _remove_background(self, depth_image: np.ndarray, binary: bool) -> np.ndarray:
        """
        remove the background from a depth image
        :param depth_image:
        :param binary: shall the response be a binary image
        :return:
        """
        if depth_image.shape != self._max_image.shape:
            raise ValueError
        return np.where(
            (depth_image > self._max_image - self._max_image_tolerance) | (
                    depth_image <= 0) | (
                    depth_image > self._dead_earth_space), 0, 1 if binary else depth_image)

    def _activate_cell(self, index, square_cropped_depth):
        """
        activate a specific cell
        :param index:
        :param square_cropped_depth:
        """
        relevant_depth_values = square_cropped_depth[
            np.nonzero(square_cropped_depth > self._dead_sky_space)]
        if len(np.transpose(relevant_depth_values)) > 0:
            minval = np.min(relevant_depth_values)
        else:
            minval = 9000
        self._active_cells.append((index, minval))
        self._current_gap = 0

    def _handle_end_of_activations(self, date: datetime.datetime, filewriter) -> bool:
        """
        calculate wheter a detection is present after a huge gap occured
        :param date:
        :param filewriter:
        :return:
        """
        detection_occured = False
        if len(self._active_cells) > 0 \
                and (self._active_cells[-1][0] - self._active_cells[0][
            0] + 1) >= 8 / 106 * utils.NUMBER_OF_CELLS \
                and len(self._active_cells) >= 5 / 106 * utils.NUMBER_OF_CELLS \
                and len(self._active_cells) / (
                self._active_cells[-1][0] - self._active_cells[0][0] + 1) >= 0.8:
            self._detections.append((self._active_cells[0][0], self._active_cells[-1][0]))
            is_same = False
            for previous_detection in self._previous_detections:
                is_same = (self._active_cells[0][0] <= previous_detection[0] <
                           self._active_cells[-1][0]) \
                          or (self._active_cells[0][0] < previous_detection[1] <=
                              self._active_cells[-1][0]) \
                          or (self._active_cells[0][0] >= previous_detection[0] and
                              self._active_cells[-1][0] <=
                              previous_detection[1])
                if is_same:
                    break
            if not is_same:
                self._generate_detection(date, filewriter)
                detection_occured = True
        self._active_cells.clear()
        return detection_occured

    def _generate_detection(self, date: datetime.datetime, filewriter):
        """
        generate a detection from activations
        :param date:
        :param filewriter:
        """
        left_pixel = self._active_cells[0][0] * utils.WIDTH / utils.NUMBER_OF_CELLS
        right_pixel = (self._active_cells[-1][
                           0] + 1) * utils.WIDTH / utils.NUMBER_OF_CELLS
        center_pixel = int(left_pixel + (
                right_pixel - left_pixel) / 2)
        min_depth = np.min([a[1] for a in self._active_cells])
        height = (min_depth * self._depth_scale)

        ratio_left = utils.calculate_ratio(self._REF_POINTS[0][0][1][0], height,
                                           self._DISTANCE_TO_EARTH)
        ratio_right = utils.calculate_ratio(self._REF_POINTS[1][0][1][0], height,
                                            self._DISTANCE_TO_EARTH)

        p1x = int((1 - ratio_left) * utils.WIDTH / 2)
        p2x = int(utils.WIDTH - (1 - ratio_right) * utils.WIDTH / 2)
        center_releative = (center_pixel - p1x) / (p2x - p1x)
        print(
            F"Time: {date}, "
            F"Distance: {self._DISTANCE_TO_EARTH - (min_depth * self._depth_scale) * 100}, "
            F"Center: {center_pixel}, "
            F"Position: {center_releative}")
        filewriter.writerow(
            [date.timestamp(),
             self._DISTANCE_TO_EARTH - (min_depth * self._depth_scale) * 100,
             center_releative])

    def update_images(self, image_in: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
        """
        update the saved images and the background image with a new one
        :param image_in:
        :return: depth image and binary image with removed background
        """
        self._images_array.append(image_in)
        max_image_temp = np.zeros((self.image_y_max - self.image_y_min, utils.WIDTH), dtype=int)
        max_image = max_image_temp
        if len(self._images_array) >= 2:
            for image in self._images_array:
                max_image_temp = np.fmax(max_image_temp, image)
            max_image = max_image_temp
            if len(self._images_array) > 15:
                self._images_array.pop(0)
        self._max_image = max_image
        self._depth_image = self._remove_background(image_in, False)
        self._binary_image = self._remove_background(image_in, True)
        return self._depth_image, self._binary_image

    def do_detection(self, date: datetime.datetime, filewriter) -> bool:
        """
        do the detection. update_images has to be called before
        :param date:
        :param filewriter:
        :return:
        """
        detection_occured = False
        self._detections.clear()
        for index, sensor_sqare in enumerate(self._SENSOR):
            square_cropped_depth = self._depth_image[
                                   sensor_sqare[0][1]:sensor_sqare[1][1],
                                   sensor_sqare[0][0]:sensor_sqare[1][0]]
            square_cropped_thresh = self._binary_image[
                                    sensor_sqare[0][1]:sensor_sqare[1][1],
                                    sensor_sqare[0][0]:sensor_sqare[1][0]]
            active = np.sum(square_cropped_thresh) > \
                     (utils.WIDTH / utils.NUMBER_OF_CELLS) ** 2 \
                     * utils.CELL_HEIGHT_FACTOR * utils.ACTIVATION_THRESHOLD
            if active:
                self._activate_cell(index, square_cropped_depth)
            elif self._current_gap >= 2 / 106 * utils.NUMBER_OF_CELLS:
                detection_occured = detection_occured or self._handle_end_of_activations(date,
                                                                                         filewriter)
            else:
                self._current_gap += 1
        self._previous_detections.clear()
        for detection in self._detections:
            self._previous_detections.append(detection)
            return detection_occured
