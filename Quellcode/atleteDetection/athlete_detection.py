"""
entrypoint to do detection from a file
"""
import argparse
import csv
import os.path
import sys
from datetime import datetime

import cv2
import numpy as np
import pyrealsense2 as rs

import detection_utils as utils
from detector import Detector

parser = argparse.ArgumentParser(description="Read recorded bag file and display depth stream in jet colormap.\
                                Remember to change the stream fps and format to match the recorded.")
parser.add_argument("-i", "--input", type=str, help="Path to the bag file")
args = parser.parse_args()

if not args.input:
    print("No input paramater have been given.")
    print("For help type --help")
    sys.exit()

if os.path.splitext(args.input)[1] != ".bag":
    print("The given file is not of correct file format.")
    print("Only .bag files are accepted")
    sys.exit()

align = rs.align(rs.stream.color)
pipeline = rs.pipeline()
config = rs.config()

rs.config.enable_device_from_file(config, args.input)
FILE_NAME = args.input.split(".")[-2].split("\\")[-1]

config.enable_stream(rs.stream.depth, utils.WIDTH, utils.HEIGHT, rs.format.z16, 60)
config.enable_stream(rs.stream.color, utils.WIDTH, utils.HEIGHT, rs.format.bgr8, 60)

profile = pipeline.start(config)

depth_sensor = profile.get_device().first_depth_sensor()
depth_scale = depth_sensor.get_depth_scale()

detector = Detector(depth_scale)

try:
    with open(f"detections-{FILE_NAME}.csv", "w", newline='', encoding="utf_8") as file:
        filewriter = csv.writer(file, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        while True:

            # Wait for a coherent pair of frames: depth and color
            frames = pipeline.wait_for_frames()
            depth_frame = frames.get_depth_frame()
            color_frame = frames.get_color_frame()
            if not depth_frame or not color_frame:
                continue

            # Convert images to numpy arrays
            depth_image = np.asanyarray(depth_frame.get_data())
            color_image = np.asanyarray(color_frame.get_data())

            timestamp = color_frame.get_timestamp()

            color_image_resized = color_image[detector.image_y_min: detector.image_y_max,
                                  0:utils.WIDTH]
            depth_image_resized = depth_image[detector.image_y_min: detector.image_y_max,
                                  0:utils.WIDTH]

            depth_image_resized_bg_removed, threshold_image = detector.update_images(
                depth_image_resized)

            depth_colormap_resized = cv2.applyColorMap(cv2.convertScaleAbs(
                depth_image_resized_bg_removed, alpha=0.03), cv2.COLORMAP_JET)

            date = datetime.fromtimestamp(timestamp / 1000)

            detection_occured = detector.do_detection(date, filewriter)

            if detection_occured:
                cv2.imwrite(f'images\\{FILE_NAME}\\{date.strftime("%Y%m%d-%H%M%S%f")}.jpg',
                            color_image_resized)
                cv2.imwrite(f'images\\{FILE_NAME}\\{date.strftime("%Y%m%d-%H%M%S%f")}-depth.jpg',
                            depth_colormap_resized)
                cv2.imwrite(f'images\\{FILE_NAME}\\{date.strftime("%Y%m%d-%H%M%S%f")}-thresh.jpg',
                            threshold_image * 255)

            images = np.vstack((color_image_resized, depth_colormap_resized))

            # Show images
            cv2.namedWindow('RealSense', cv2.WINDOW_AUTOSIZE)
            cv2.imshow('RealSense', images)
            key = cv2.waitKey(1)
            # Press esc or 'q' to close the image window
            if key & 0xFF == ord('q') or key == 27:
                cv2.destroyAllWindows()
                break

finally:

    # Stop streaming
    pipeline.stop()
