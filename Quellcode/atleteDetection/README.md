# Detektionsmodul

## Installation

Es wird Python Version 3.7 und pip benötigt.

Je nach Betriebssystems das Script ``setup.bat`` oder ``setup.sh`` ausführen. In der
Datei ``setup.bat`` muss der Dateipfad zur Python-Installation angegeben werden.

Es empfiehlt sich das Intel [Realsense SDK](https://github.com/IntelRealSense/librealsense) zu
installieren. Mit dem installierten Viewer lässt sich die ``.bag``-Datei auch außerhalb dieser
Applikation öffnen.

## Ausführung

Die Detektion kann nach der Installation in der virtuellen Umgebung mit dem
Befehl ``python athlete_detection.py -i <<file-path>>`` gestartet werden. Es muss der Pfad
zur ``.bag``-Datei angegeben werden.

Die Ergebnisse werden in der Konsole ausgegeben und es wird eine ``.csv``-Datei mit dem Namen der
Eingabedatei produziert.
