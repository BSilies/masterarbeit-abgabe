"""
module for all reidentification functions
"""
from pathlib import Path

import cv2
import numpy as np
from openvino.runtime import Core

import my_types

_DEVICE = "CPU"
_MODEL_FILE = "C:/Users/Bastian/Documents/Studium/Master_Thesis/master-thesis/Code/FinalApplication/reidentificator/intel/person-reidentification-retail-0277/FP32/person-reidentification-retail-0277.xml"
_model_xml_path = Path(_MODEL_FILE)
_ie = Core()
_ie.set_property({'CACHE_DIR': '/cache'})
_model = _ie.read_model(_model_xml_path)
_compiled_model = _ie.compile_model(model=_model, device_name=_DEVICE)
_input_key = _compiled_model.input(0)
_output_key = _compiled_model.output(0)
_network_input_shape = list(_input_key.shape)
_network_image_height, _network_image_width = _network_input_shape[2:]

_persons: dict[str, my_types.SavedPersonTrackingPayload] = {}
_MAX_DISTANCE = 0.45
_MAX_DISTANCE_ANCESTORS = 0.45
_MAX_GENERATION = 1


def process(request: my_types.ReidRequest) -> dict[str, my_types.ReidentificationElement] | None:
    """
    process a reidentification request
    :param request:
    :return: reidentification result if request via request-parameter
    """
    result = None
    if request['operation'] == my_types.Operation.ADD or request[
        'operation'] == my_types.Operation.ADD_GET:
        _add(request['id'], request['data'])
    if request['operation'] == my_types.Operation.GET or request[
        'operation'] == my_types.Operation.ADD_GET:
        result = _get(request['id'])
    return result


def _add(element_id: str, payload: my_types.PersonTrackingPayload) -> None:
    """
    add new element to reidentification
    :param element_id:
    :param payload:
    """
    if payload['best_image_person'] is not None and payload['best_image_person'].shape[0] > 0 and \
            payload['best_image_person'].shape[1] > 0:

        resized_image_person = cv2.resize(src=payload['best_image_person'],
                                          dsize=(_network_image_height, _network_image_width))
        input_image_person = np.expand_dims(np.transpose(resized_image_person, (2, 0, 1)), 0)
        reid_vector_person = _compiled_model([input_image_person])[_output_key][0]
        distances: list[tuple[str, float]] = []

        for key, value in _persons.items():
            scalar_person_person = np.dot(reid_vector_person, value['reid_vector_p'])

            l1_person = np.linalg.norm(reid_vector_person)

            l2_person = np.linalg.norm(value['reid_vector_p'])

            distance_person_person = 1 - scalar_person_person / (l1_person * l2_person)
            distance_person_bib = 1000
            distance_bib_person = 1000
            distance_bib_bib = 1000

            if min(distance_person_person, distance_person_bib, distance_bib_person,
                   distance_bib_bib) < _MAX_DISTANCE:
                distances.append(
                    (key, min(distance_person_person, distance_person_bib, distance_bib_person,
                              distance_bib_bib)))
                value['distances'].append((element_id,
                                           min(distance_person_person, distance_person_bib,
                                               distance_bib_person, distance_bib_bib)))

        _persons[element_id] = my_types.SavedPersonTrackingPayload(reid_vector_p=reid_vector_person,
                                                                   image_p=payload[
                                                                       "best_image_person"],
                                                                   image_b=payload[
                                                                       "best_image_bib"],
                                                                   bibs=payload["bibs"],
                                                                   distances=distances,
                                                                   image_d=payload[
                                                                       'detection_image_person'])


def _get(element_id: str):
    """
    get reidentification data for a given element-id
    :param element_id:
    :return:
    """
    group: dict[str, my_types.ReidentificationElement] = {}
    if element_id in _persons:
        bibs = None
        if len(_persons[element_id]['bibs']) > 0:
            bibs = sorted(_persons[element_id]['bibs'], key=lambda bib: bib['rating'],
                          reverse=True)[
                   0:3]
        group[element_id] = my_types.ReidentificationElement(parent=None, distance_to_root=0.0,
                                                             generation=0,
                                                             bibs=bibs,
                                                             image_p=_persons[element_id][
                                                                 'image_p'],
                                                             image_b=_persons[element_id][
                                                                 'image_b'],
                                                             image_d=_persons[element_id][
                                                                 'image_d'])
        children = _build_group(_persons[element_id]['distances'], element_id, 1, _MAX_GENERATION,
                                0.0)
        for key, value in children.items():
            if _should_child_be_added(group, key, value):
                group[key] = value
    return sorted(group.items(), key=lambda item: item[1]["distance_to_root"])[:4]


def _build_group(children_p: list[tuple[str, float]], parent: str, generation: int,
                 max_generation: int,
                 current_distance: float):
    """
    build a collection of all relevant children
    :param children_p:
    :param parent:
    :param generation:
    :param max_generation:
    :param current_distance:
    :return:
    """
    group_to_build: dict[str, my_types.ReidentificationElement] = {}
    for current_child in children_p:
        bibs_temp = _get_relevant_bibs(current_child)
        if _is_child_relevant_to_add(current_child, current_distance, group_to_build):
            group_to_build[current_child[0]] = \
                my_types.ReidentificationElement(parent=parent,
                                                 distance_to_root=current_distance + current_child[
                                                     1],
                                                 generation=generation,
                                                 bibs=bibs_temp,
                                                 image_p=_persons[current_child[0]]['image_p'],
                                                 image_b=_persons[current_child[0]]['image_b'],
                                                 image_d=_persons[current_child[0]]['image_d'])
    if generation < max_generation:
        for current_child in children_p:
            return_value = _build_group(_persons[current_child[0]]['distances'], current_child[0],
                                        generation + 1,
                                        max_generation, current_distance + current_child[1])
            for key, value in return_value.items():
                if _should_child_be_added(group_to_build, key, value):
                    group_to_build[key] = value
    return group_to_build


def _get_relevant_bibs(current_child: tuple[str, float]) -> list[my_types.BibTracking] | None:
    """
    get the three best bibs for an entry
    :param current_child:
    :return:
    """
    return sorted(_persons[current_child[0]]['bibs'], key=lambda bib: bib['rating'],
                  reverse=True)[0:3] if len(_persons[current_child[0]]['bibs']) > 0 else None


def _should_child_be_added(group_to_build: dict[str, my_types.ReidentificationElement],
                           key: str, value: my_types.ReidentificationElement) -> bool:
    """
    check whether a child should be added to the collection of reidentification-elements
    :param group_to_build:
    :param key:
    :param value:
    :return:
    """
    return list(group_to_build).count(key) == 0 or 0 < value['distance_to_root'] < \
        group_to_build[key]['distance_to_root']


def _is_child_relevant_to_add(current_child: tuple[str, float], current_distance: float,
                              group_to_build: dict[str, my_types.ReidentificationElement]) -> bool:
    """
    check whether a child fulfills the conditions to be added
    :param current_child:
    :param current_distance:
    :param group_to_build:
    :return:
    """
    return list(group_to_build).count(current_child[0]) == 0 and current_distance + \
        current_child[1] < _MAX_DISTANCE_ANCESTORS or 0 < int(
            current_distance + current_child[1]) * 1000 < int(
            group_to_build[current_child[0]]['distance_to_root']) * 1000
