# Reidentifikationsmodul

## Installation

Es wird Python Version 3.10 und pip benötigt.

Je nach Betriebssystems das Script ``setup.bat`` oder ``setup.sh`` ausführen. In der
Datei ``setup.bat`` muss der Dateipfad zur Python-Installation angegeben werden.

## Ausführung

Der Server kann nach der Installation in der virtuellen Umgebung mit dem
Befehl ``python app.py`` gestartet werden.

Der Server beendet sich nicht von selbst, sondern muss aktiv beendet werden.