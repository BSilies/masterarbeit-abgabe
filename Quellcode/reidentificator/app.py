"""
entrypoint for reidentification
"""
import asyncio
import logging
import pickle

import websockets
import websockets.server

import reidentificator


async def handler(websocket):
    """
    handles reidentification-requests
    :param websocket:
    """
    async for message in websocket:
        result = reidentificator.process(pickle.loads(message))
        if result is not None:
            await websocket.send(pickle.dumps(result))


async def main():
    """
    starts a forever running server
    """
    port = 8005
    logger = logging.getLogger()
    async with websockets.serve(handler, "", port, logger=logger, max_size=2 ** 25):
        await asyncio.Future()  # run forever


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    asyncio.run(main())
