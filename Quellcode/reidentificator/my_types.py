import datetime
import typing
from enum import Enum

import numpy as np


class Bib(typing.TypedDict):
    text: str
    confidence: float


class BibTracking(Bib):
    seen: int
    rating: float


class PersonTrackingPayload(typing.TypedDict):
    bibs: list[BibTracking]
    best_image_bib: np.ndarray | None
    best_image_person: np.ndarray | None
    detection_image_person: np.ndarray | None
    person_confidence: float | None
    timestamp_best_image: float
    timestamp_detection: datetime.datetime | None


class SavedPersonTrackingPayload(typing.TypedDict):
    bibs: list[BibTracking]
    image_p: np.ndarray
    reid_vector_p: list[float]
    distances: list[tuple[str, float]]
    image_b: np.ndarray | None
    image_d: np.ndarray | None


class Operation(Enum):
    ADD = 1
    GET = 2
    ADD_GET = 3


class ReidRequest(typing.TypedDict):
    operation: Operation
    id: str
    data: PersonTrackingPayload | None


class ReidentificationElement(typing.TypedDict):
    parent: str | None
    bibs: list[BibTracking] | None
    distance_to_root: float
    generation: int
    image_p: np.ndarray | None
    image_b: np.ndarray | None
    image_d: np.ndarray | None
