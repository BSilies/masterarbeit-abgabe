"""
some functions regarding bounding boxes
"""
import my_types


def convert_bbox_to_center_format(bbox: my_types.BoundingBoxEdges) -> my_types.BoundingBoxCenter:
    """
    convert bbox from (l,t,r,b) to (y,x,w,h)
    :param bbox:
    :return:
    """
    center_x = (bbox["left"] + bbox["right"]) / 2
    center_y = (bbox["top"] + bbox["bottom"]) / 2
    width = bbox["right"] - bbox["left"]
    height = bbox["bottom"] - bbox["top"]
    return my_types.BoundingBoxCenter(x=center_x, y=center_y, width=width, height=height)


def convert_center_format_to_bbox(
        center_format: my_types.BoundingBoxCenter) -> my_types.BoundingBoxEdges:
    """
    convert bbox from (y,x,w,h) to (l,t,r,b)
    :param bbox:
    :return:
    """
    top = center_format["y"] - center_format["height"] / 2
    bottom = center_format["y"] + center_format["height"] / 2
    left = center_format["x"] - center_format["width"] / 2
    right = center_format["x"] + center_format["width"] / 2
    return my_types.BoundingBoxEdges(left=left, top=top, right=right, bottom=bottom)


def has_intersection(
        bbox1: my_types.BoundingBoxCenter, bbox2: my_types.BoundingBoxCenter | None) -> \
        tuple[bool, float, float]:
    """
    check whether bboxes are intersecting
    :param bbox1:
    :param bbox2:
    :return: (has intersection, ratio to bbox1, ratio to bbox2)
    """
    if bbox1 is None or bbox2 is None:
        return bool(False), 0.0, 0.0
    width_combined = bbox1["width"] + bbox2["width"]
    height_combined = bbox1["height"] + bbox2["height"]

    low_boundary_intersection_x = bbox1["x"] - width_combined / 2
    high_boundary_intersection_x = bbox1["x"] + width_combined / 2
    low_boundary_intersection_y = bbox1["y"] - height_combined / 2
    high_boundary_intersection_y = bbox1["y"] + height_combined / 2

    intersection = low_boundary_intersection_x <= bbox2["x"] <= high_boundary_intersection_x \
                   and low_boundary_intersection_y <= bbox2["y"] <= high_boundary_intersection_y

    intersection_width: float = 0.0
    intersection_height: float = 0.0

    if intersection:

        width_difference = bbox1["width"] - bbox2["width"]
        height_difference = bbox1["height"] - bbox2["height"]

        low_boundary_overlapping_x = bbox1["x"] - width_difference / 2
        high_boundary_overlapping_x = bbox1["x"] + width_difference / 2
        low_boundary_overlapping_y = bbox1["y"] - height_difference / 2
        high_boundary_overlapping_y = bbox1["y"] + height_difference / 2

        if low_boundary_overlapping_x > high_boundary_overlapping_x and \
                low_boundary_overlapping_x >= bbox2["x"] >= high_boundary_overlapping_x:
            intersection_width = bbox1["width"]
        elif high_boundary_overlapping_x >= bbox2["x"] >= low_boundary_overlapping_x:
            intersection_width = bbox2["width"]
        elif low_boundary_overlapping_x > bbox2["x"] >= low_boundary_intersection_x:
            intersection_width = bbox2["x"] - low_boundary_intersection_x
        elif high_boundary_overlapping_x < bbox2["x"] <= high_boundary_intersection_x:
            intersection_width = high_boundary_intersection_x - bbox2["x"]
        else:
            intersection_width = 0.0

        if low_boundary_overlapping_y > high_boundary_overlapping_y and \
                low_boundary_overlapping_y >= bbox2["y"] >= high_boundary_overlapping_y:
            intersection_height = bbox1["width"]
        elif high_boundary_overlapping_y >= bbox2["y"] >= low_boundary_overlapping_y:
            intersection_height = bbox2["width"]
        elif low_boundary_overlapping_y > bbox2["y"] >= low_boundary_intersection_y:
            intersection_height = bbox2["y"] - low_boundary_intersection_y
        elif high_boundary_overlapping_y < bbox2["y"] <= high_boundary_intersection_y:
            intersection_height = high_boundary_intersection_y - bbox2["y"]
        else:
            intersection_height = 0.0

    intersection_area = intersection_height * intersection_width
    bbox1_area = bbox1["width"] * bbox1["height"]
    bbox2_area = bbox2["width"] * bbox2["height"]

    return (intersection, intersection_area / bbox1_area if bbox1_area != 0 else 0.0,
            intersection_area / bbox2_area if bbox2_area != 0 else 0.0)


def has_intersections_generator(bbox: my_types.BoundingBoxCenter,
                                bboxes: list[my_types.BoundingBoxTracking]):
    """
    just a generator
    :param bbox:
    :param bboxes:
    """
    for box in bboxes:
        if box is None:
            yield has_intersection(bbox, None)
        else:
            yield has_intersection(bbox, box['bounding_box'])
