"""
utils especially for the finish camera
"""
import datetime
import math

import cv2

FACTOR_WORK = 1
FACTOR_SHOW = 0.5
FACTOR_WORK_TO_SHOW = FACTOR_SHOW / FACTOR_WORK

START_TIME = datetime.datetime(year=2023, day=17, month=2, hour=18, minute=35, second=32, microsecond=630000)

# Ref1: Erde = 0, Ref2: 148.8
REF_POINTS = [((848, 847), (838, 508)), ((1137, 901), (1130, 493))]
side_a = 138
side_b = 385
side_c = 300
angle_BAC = math.acos((side_b ** 2 + side_c ** 2 - side_a ** 2) / (2 * side_b * side_c))
angle_CBA = math.acos((side_a ** 2 + side_c ** 2 - side_b ** 2) / (2 * side_a * side_c))
angle_C2BA = math.pi / 2 - angle_BAC / 2
angle_AC2B = angle_C2BA
side_a1 = side_c * math.sin(angle_BAC) / math.sin(angle_AC2B)


async def calculate_head_points_for_detections(frame_out, head_points, last_time, my_queue,
                                               next_detection, play_time):
    """
    calculate head points from detection
    :param frame_out:
    :param head_points:
    :param last_time:
    :param my_queue:
    :param next_detection:
    :param play_time:
    """
    while last_time < next_detection[0] <= play_time:

        side_x = (1 - next_detection[2]) * side_a
        side_b1 = math.sqrt(side_x ** 2 + side_c ** 2 - 2 * side_x * side_c * math.cos(angle_CBA))
        angle_BAC1 = math.asin((side_x * math.sin(angle_C2BA)) / side_b1)
        side_x1 = side_c * math.sin(angle_BAC1) / math.sin(math.pi - (angle_BAC1 + angle_C2BA))

        x_b = int((REF_POINTS[1][0][0] + (
                REF_POINTS[0][0][0] - REF_POINTS[1][0][0]) * side_x1 / side_a1) * FACTOR_SHOW)
        y_b = int((REF_POINTS[1][0][1] + (
                REF_POINTS[0][0][1] - REF_POINTS[1][0][1]) * side_x1 / side_a1) * FACTOR_SHOW)
        x_t = int((REF_POINTS[1][1][0] + (
                REF_POINTS[0][1][0] - REF_POINTS[1][1][0]) * side_x1 / side_a1) * FACTOR_SHOW)
        y_t = int((REF_POINTS[1][1][1] + (
                REF_POINTS[0][1][1] - REF_POINTS[1][1][1]) * side_x1 / side_a1) * FACTOR_SHOW)
        x_h = int(x_b + (x_t - x_b) / 148.8 * next_detection[1])
        y_h = int(y_b + (y_t - y_b) / 148.8 * next_detection[1])
        head_points.append(
            [next_detection[0], (x_h / FACTOR_WORK_TO_SHOW, y_h / FACTOR_WORK_TO_SHOW), [], 0])

        cv2.circle(frame_out, (x_h, y_h), 3, (0, 0, 255), -1)
        cv2.line(frame_out, (x_b, y_b), (x_t, y_t), (0, 0, 255), 2)

        if not my_queue.empty():
            next_detection = my_queue.get()
        else:
            break
    return next_detection


async def match_detections_to_persons(fps, frame_out, head_points, head_to_remove,
                                      person_bounding_boxes):
    """
    match calculated head points to detected persons
    :param fps:
    :param frame_out:
    :param head_points:
    :param head_to_remove:
    :param person_bounding_boxes:
    """
    for idx, head in enumerate(head_points):
        if len(head[2]) >= 1:
            head[2].sort(key=lambda x: abs(person_bounding_boxes[x]["top"] - head[1][1]))
            person_bounding_boxes[head[2][0]]['time'] = head[0]
            head_to_remove.append(idx)
            img = frame_out[
                  int(person_bounding_boxes[head[2][0]]['top'] * FACTOR_WORK_TO_SHOW):int(
                      person_bounding_boxes[head[2][0]]['bottom'] * FACTOR_WORK_TO_SHOW),
                  int(person_bounding_boxes[head[2][0]]['left'] * FACTOR_WORK_TO_SHOW):int(
                      person_bounding_boxes[head[2][0]]['right'] * FACTOR_WORK_TO_SHOW)]
            cv2.imwrite(
                f'generatedData/images/{head[0].strftime("%Y%m%d-%H%M%S%f")}-{head[2][0]}.jpg', img)
        else:
            print("Miss")
            head[3] += 1
            if head[3] >= round(fps / 10):
                head_to_remove.append(idx)
                print(F"No person found for time {head[0].isoformat()}")


def is_head_point_in_bounding_box(bbox, bbox_in, head):
    """
    check whether head point is in valid area of a person bounding box
    :param bbox:
    :param bbox_in:
    :param head:
    :return:
    """
    return bbox['left'] + bbox_in['bounding_box']['width'] / 8 < head[1][0] < bbox['right'] - \
        bbox_in['bounding_box']['width'] / 8 \
        and bbox['top'] - bbox_in['bounding_box']['height'] / 8 < head[1][1] < bbox['top'] + \
        bbox_in['bounding_box']['height'] / 3
