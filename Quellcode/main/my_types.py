import datetime
import typing
from enum import Enum

import numpy as np


class BoundingBoxCenter(typing.TypedDict):
    x: float
    y: float
    width: float
    height: float


class BoundingBoxEdges(typing.TypedDict):
    left: float
    top: float
    right: float
    bottom: float


class Point(typing.TypedDict):
    x: float
    y: float


class BibDetection(typing.TypedDict):
    bbox: list[Point]
    text: str
    confidence: float


class Detection(typing.TypedDict):
    confidence: float
    bounding_box: BoundingBoxCenter
    bib: BibDetection | None


class LonelyBib(typing.TypedDict):
    bib: Detection
    seen: int
    last_seen: int


class PersonBoundingBox(BoundingBoxEdges):
    confidence: float
    time: datetime.datetime | None


class DetectionYOLO(typing.TypedDict):
    confidence: float
    bounding_box: BoundingBoxCenter


class Bib(typing.TypedDict):
    text: str
    confidence: float


class BibTracking(Bib):
    seen: int
    rating: float


class PersonTrackingPayload(typing.TypedDict):
    bibs: list[BibTracking]
    best_image_bib: np.ndarray | None
    best_image_person: np.ndarray | None
    detection_image_person: np.ndarray | None
    person_confidence: float | None
    timestamp_best_image: float
    timestamp_detection: datetime.datetime | None


class SeenItem(typing.TypedDict):
    tracking_key: int
    bounding_box: BoundingBoxCenter
    confidence: float


class BoundingBoxTracking(typing.TypedDict):
    bounding_box: BoundingBoxCenter
    confidence: float


class GuessParams(typing.TypedDict):
    fit_x: tuple[np.ndarray, typing.Any, np.ndarray]
    fit_y: tuple[np.ndarray, typing.Any, np.ndarray]
    fit_w: tuple[np.ndarray, typing.Any, np.ndarray]
    fit_h: tuple[np.ndarray, typing.Any, np.ndarray]


class TrackingData(typing.TypedDict):
    id: int
    bounding_boxes: list[BoundingBoxTracking | None]
    first_bounding_box: BoundingBoxTracking
    guess_next: BoundingBoxCenter | None
    guess_params: GuessParams | None
    payload: PersonTrackingPayload | None


class Operation(Enum):
    ADD = 1
    GET = 2
    ADD_GET = 3


class ReidRequest(typing.TypedDict):
    operation: Operation
    id: str
    data: PersonTrackingPayload | None


class ReidentificationElement(typing.TypedDict):
    parent: str | None
    bibs: list[BibTracking] | None
    distance_to_root: float
    generation: int
    image_p: np.ndarray | None
    image_b: np.ndarray | None
