import asyncio
import glob
import logging
import os
import pickle

import cv2
import numpy as np
import websockets
from matplotlib import pyplot as plt

import my_types


async def main():
    input_path_reid = "../generatedData/reid/validation"
    input_files_reid = glob.glob(os.path.join(input_path_reid, "*.pickle"))

    detection_ids: list[str] = []

    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger()
    uri = "ws://localhost:8005"

    async with websockets.connect(uri, logger=logger, max_size=2 ** 25) as websocket:
        for input_file in input_files_reid:
            detection_id = input_file.split(".")[-2].split("\\")[-1]
            detection_ids.append(detection_id)

        for index, detection_id in enumerate(detection_ids):
            request: my_types.ReidRequest = my_types.ReidRequest(id=detection_id, data=None,
                                                                 operation=my_types.Operation.GET)
            await websocket.send(pickle.dumps(request))
            await asyncio.sleep(0)
            group: list[tuple[str, my_types.ReidentificationElement]] = pickle.loads(
                await websocket.recv())
            await asyncio.sleep(0)
            if len(group) <= 25:
                fig = plt.figure(figsize=(15, 8), num=detection_id)
                rows = int(len(group) / 5)
                if len(group) % 5 != 0:
                    rows += 1
                columns = int(len(group) % 5) if len(group) < 5 else 5
                counter = 1
                for idx, item in sorted(group, key=lambda item: item[1]["distance_to_root"]):
                    bib = ""
                    fig.add_subplot(rows, columns, counter)
                    counter += 1
                    plt.imshow(cv2.cvtColor(item["image_p"], cv2.COLOR_BGR2RGB))
                    plt.axis('off')
                    if item['bibs'] is not None and item['bibs']:
                        bib = F" - {item['bibs'][0]['text']} ({item['bibs'][0]['seen']})"
                    plt.title(F"{idx} - {item['generation']}{bib}")
                    plt.text(0, 0.9, item['distance_to_root'],
                             bbox={'facecolor': 'white', 'alpha': 0.5})
                fig.suptitle(F"{detection_id} ({index + 2})")
                plt.show()


if __name__ == "__main__":
    asyncio.run(main())
