import asyncio
import glob
import logging
import os
import pickle

import websockets

import my_types


async def main():
    input_path_reid = "../generatedData/reid/test"
    input_files_reid = glob.glob(os.path.join(input_path_reid, "*.pickle"))

    detection_ids: list[str] = []

    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger()
    uri = "ws://localhost:8005"

    async with websockets.connect(uri, logger=logger, max_size=2 ** 25) as websocket:
        requests = []
        for input_file in input_files_reid:
            detection_id = input_file.split(".")[-2].split("\\")[-1]
            detection_ids.append(detection_id)
            with open(input_file, "rb") as file:
                data: my_types.PersonTrackingPayload = pickle.load(file)
            request: my_types.ReidRequest = my_types.ReidRequest(id=detection_id, data=data,
                                                                 operation=my_types.Operation.ADD)
            requests.append(request)
        for request in requests:
            await websocket.send(pickle.dumps(request))
            await asyncio.sleep(0)


if __name__ == "__main__":
    asyncio.run(main())
