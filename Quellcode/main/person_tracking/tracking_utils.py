"""
some functions related to tracking
"""
import asyncio
import pickle

import numpy as np

import my_types
from person_tracking.result_calculation import calculate_result


def calculate_guess_bounding_box_values(fit_h: list[float], fit_w: list[float], fit_x: list[float],
                                        fit_y: list[float], height: int, width: int) -> \
        tuple[float, float, float, float]:
    """
    calculate a bounding box prediction from functional parameters
    :param fit_h:
    :param fit_w:
    :param fit_x:
    :param fit_y:
    :param height:
    :param width:
    :return: the parameters of the bounding box (h, w, x, y)
    """
    x_guess = ((1 + np.tanh(-fit_x[0] * 10 + fit_x[1])) * (width / 2))
    y_guess = ((1 + np.tanh(-fit_y[0] * 10 + fit_y[1])) * (height / 2))
    w_guess = ((1 + np.tanh(-fit_w[0] * 3 + fit_w[1])) * (width / 2))
    h_guess = ((1 + np.tanh(-fit_h[0] * 3 + fit_h[1])) * (height / 2))
    return h_guess, w_guess, x_guess, y_guess


def calculate_fitting_parameters(height_values: list[float], indices: list[int],
                                 width_values: list[float], x_values: list[float],
                                 y_values: list[float]):
    """
    calculate functional parameters from historic data using an arctan function
    :param height_values:
    :param indices:
    :param width_values:
    :param x_values:
    :param y_values:
    :return:
    """
    fit_x = np.polyfit(indices, np.arctanh(x_values), 1)
    fit_y = np.polyfit(indices, np.arctanh(y_values), 1)
    fit_w = np.polyfit(indices, np.arctanh(width_values), 1)
    fit_h = np.polyfit(indices, np.arctanh(height_values), 1)
    return fit_h, fit_w, fit_x, fit_y


async def process_closed_tracking_data(tracking_item: my_types.TrackingData, source: str,
                                       websocket) -> None:
    """
    send tracking data to reidentification and calculate a result if necessary
    :param tracking_item:
    :param source:
    :param websocket:
    """
    request: my_types.ReidRequest = my_types.ReidRequest(
        id=F"{source.split('.')[0].split('/')[-1]}-{tracking_item['id']}",
        data=tracking_item['payload'],
        operation=my_types.Operation.ADD if tracking_item['payload'][
                                                'timestamp_detection'] is None else my_types.Operation.ADD_GET)
    await websocket.send(pickle.dumps(request))
    await asyncio.sleep(0)
    if tracking_item['payload']['timestamp_detection'] is not None:
        group: list[tuple[str, my_types.ReidentificationElement]] = pickle.loads(
            await websocket.recv())
        await asyncio.sleep(0)
        await calculate_result(group, tracking_item)
