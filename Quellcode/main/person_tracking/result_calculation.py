"""
everything about calculating a result for a tracking entry
"""
import math

import numpy as np

import my_types


async def calculate_result(group: list[tuple[str, my_types.ReidentificationElement]],
                           tracking_item: my_types.TrackingData) -> None:
    """
    calculate a result from given tracking item and reidentification data
    :param group:
    :param tracking_item:
    """

    if group is not None and group and group[0][1]['bibs'] is not None and group[0][1]['bibs'] and \
            group[0][1]['bibs'][0]['rating'] >= 0.7:
        print(
            f"Zeit: {tracking_item['payload']['timestamp_detection']}, "
            f"Startnummer: {group[0][1]['bibs'][0]['text']} - "
            f"{group[0][1]['bibs'][0]['rating']:1.2} (Direkt)")
    elif group is not None and group:
        bibs = await aggregate_all_bibs_from_reid(group)
        if len(bibs.keys()) > 0:
            max_count = np.max([count[0] for count, _ in bibs.values()])
            if max_count >= 2:
                max_bibs = [bib for bib, (count, _) in bibs.items() if count == max_count]
                max_bibs_ratings = []
                for bib in max_bibs:
                    max_bibs_ratings.append(
                        bibs[bib][0][0] * math.prod(bibs[bib][1]) / sum(bibs[bib][1]))
                if len(max_bibs) > 1:
                    index = max_bibs_ratings.index(np.max(max_bibs_ratings))
                    print(
                        f"Zeit: {tracking_item['payload']['timestamp_detection']}, "
                        f"Startnummer: {max_bibs[index]} - "
                        f"{max_bibs_ratings[index]} "
                        f"(In {max_count}/{len(group)} Elementen nach Auswahl)")
                else:
                    print(
                        f"Zeit: {tracking_item['payload']['timestamp_detection']}, "
                        f"Startnummer: {max_bibs[0]} - "
                        f"{max_bibs_ratings[0]} "
                        f"(In {max_count}/{len(group)} Elementen)")


async def aggregate_all_bibs_from_reid(
        group: list[tuple[str, my_types.ReidentificationElement]]) -> \
        dict[str, tuple[list[int], list[float]]]:
    """
    aggregate all bibs from all reidentification results
    :param group:
    :return: list of bibs
    """
    bibs: dict[str, tuple[list[int], list[float]]] = {}
    for idx, group_item in enumerate(group):
        if group_item[1]['bibs'] is None:
            continue
        for bib in group_item[1]['bibs']:
            if (bib['rating'] if idx != 0 else bib['rating'] * 1.1) >= 0.6:
                if bib['text'] in bibs:
                    bibs[bib['text']][0][0] += 1
                    bibs[bib['text']][1].append(
                        bib['rating'] if idx != 0 else bib['rating'] * 1.1)
                else:
                    bibs[bib['text']] = (
                        [1], [bib['rating'] if idx != 0 else bib['rating'] * 1.1])
    return bibs
