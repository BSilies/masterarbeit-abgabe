"""
the entrypoint to tracking
"""
import numpy as np

import my_types
import person_tracking.tracker


def track_person(frame: np.ndarray, height: int, person_bounding_box: my_types.PersonBoundingBox,
                 tracking_ids: list[int], width: int,
                 person_tracker: person_tracking.tracker.ObjectTracker, timestamp: float) -> None:
    """
    do the tracking for one bounding box
    :param frame:
    :param height:
    :param person_bounding_box:
    :param tracking_ids:
    :param width:
    :param person_tracker:
    :param timestamp:
    """
    tracking_id = person_tracker.track(person_bounding_box, height, width)
    tracking_ids.append(tracking_id)
    person_payload = person_tracker.get_payload(tracking_id)

    if person_payload is None:
        person_payload = my_types.PersonTrackingPayload(bibs=[], person_confidence=None,
                                                       best_image_person=None,
                                                       best_image_bib=None,
                                                       timestamp_best_image=timestamp,
                                                       detection_image_person=None,
                                                       timestamp_detection=None)
        person_tracker.set_payload(tracking_id, person_payload)

    if is_current_image_better_than_saved(person_bounding_box, person_payload):
        cropped_image: np.array = frame[
                                  int(person_bounding_box["top"]):int(
                                      person_bounding_box["bottom"]),
                                  int(person_bounding_box["left"]):int(
                                      person_bounding_box["right"])].copy()
        person_payload["person_confidence"] = person_bounding_box["confidence"] / 100
        person_payload["best_image_person"] = cropped_image
        person_payload["timestamp_best_image"] = timestamp

    if person_bounding_box['time'] is not None and person_payload["timestamp_detection"] is None:
        cropped_image: np.array = frame[
                                  int(person_bounding_box["top"]):int(
                                      person_bounding_box["bottom"]),
                                  int(person_bounding_box["left"]):int(
                                      person_bounding_box["right"])].copy()
        person_payload['detection_image_person'] = cropped_image
        person_payload["timestamp_detection"] = person_bounding_box['time']


def is_current_image_better_than_saved(person_bounding_box: my_types.PersonBoundingBox,
                                       person_payload: my_types.PersonTrackingPayload | None) -> \
        bool:
    """
    decide whether the current bounding box should be saved as image
    :param person_bounding_box:
    :param person_payload:
    :return:
    """
    return (person_payload["person_confidence"] is None or
            person_payload["person_confidence"] < person_bounding_box["confidence"] / 100) and \
        person_bounding_box["right"] - person_bounding_box["left"] > 0 and \
        person_bounding_box["bottom"] - person_bounding_box["top"] > 0
