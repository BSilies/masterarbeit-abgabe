"""
some utils for tracking payload
"""
import math

import my_types

MAX_LENGTH_OF_SAVED_BOUNDING_BOXES = 60


def add_bib(payload: my_types.PersonTrackingPayload, bib: my_types.Bib) -> bool:
    """
    adds a bib to the payload or counts up thee seen-value if the bib is already present
    :param payload:
    :param bib:
    :return: True if a new bib has been added, else false
    """
    contains_bib_index = [idx for idx, x in enumerate(payload['bibs']) if x['text'] == bib['text']]
    if len(payload['bibs']) == 0 or payload['bibs'][0]['confidence'] < bib['confidence']:
        if len(contains_bib_index) > 0:
            bib_temp = payload['bibs'].pop(contains_bib_index[0])
            bib_insert = my_types.BibTracking(text=bib['text'], confidence=bib['confidence'],
                                              seen=bib_temp["seen"] + 1,
                                              rating=calc_bib_rating(bib["confidence"],
                                                                     bib_temp["seen"] + 1))
        else:
            bib_insert = my_types.BibTracking(text=bib['text'], confidence=bib['confidence'],
                                              seen=1,
                                              rating=calc_bib_rating(bib["confidence"], 1))
        payload['bibs'].append(bib_insert)
        return True

    if len(contains_bib_index) > 0:
        bib_temp = payload['bibs'][contains_bib_index[0]]
        bib_temp['seen'] += 1
        bib_temp['rating'] = calc_bib_rating(bib_temp["confidence"], bib_temp["seen"])
        return False

    bib_insert = my_types.BibTracking(text=bib['text'], confidence=bib['confidence'], seen=1,
                                      rating=calc_bib_rating(bib["confidence"], 1))
    payload['bibs'].append(bib_insert)
    return False


def calc_bib_rating(confidence: float, seen: int) -> float:
    """
    calculate the bib rating from confidence and seen-amount
    :param confidence:
    :param seen:
    :return: bib rating
    """
    sigmoid = 1.5 * (1 - 1 / (math.exp(seen / 4) + 1)) - 0.25
    return sigmoid * confidence


def add_bounding_box(tracking_data: my_types.TrackingData,
                     bounding_box: my_types.BoundingBoxCenter | None,
                     confidence: float) -> None:
    """
    add a new bounding-box to the tracking
    :param tracking_data:
    :param bounding_box:
    :param confidence:
    """
    if bounding_box is None:
        tracking_data["bounding_boxes"].insert(0, bounding_box)
    else:
        tracking_data["bounding_boxes"].insert(0, my_types.BoundingBoxTracking(
            bounding_box=bounding_box,
            confidence=confidence))
    if len(tracking_data["bounding_boxes"]) > MAX_LENGTH_OF_SAVED_BOUNDING_BOXES:
        tracking_data["bounding_boxes"].pop()
