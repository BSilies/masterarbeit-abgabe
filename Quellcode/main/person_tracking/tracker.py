"""
everything related to the tracker
"""
import asyncio
import math
import threading

import bounding_box_utils
import my_types
from person_tracking import person_tracking_payload_utils
from person_tracking.person_tracking_payload_utils import add_bounding_box, \
    MAX_LENGTH_OF_SAVED_BOUNDING_BOXES
from person_tracking.tracking_utils import calculate_guess_bounding_box_values, \
    calculate_fitting_parameters, process_closed_tracking_data

lock = threading.Lock()


class ObjectTracker:
    """
    An Object designed for tracking bounding boxes in an Image
    """

    def __init__(self) -> None:
        self._last_seen_items = []
        self.tracker_dict = {}

    _tracker_index: int = 0
    _last_seen_items: list[my_types.SeenItem]
    tracker_dict: dict[int, my_types.TrackingData]

    def _is_candidate(self, bbox: my_types.BoundingBoxCenter,
                      bboxes: list[my_types.BoundingBoxTracking | None],
                      tracker_key: int, height: int, width: int) -> bool:
        """
        decide whether one element from tracking is a candidate for a bounding box
        :param bbox:
        :param bboxes:
        :param tracker_key:
        :param height:
        :param width:
        :return:
        """
        indices: list[int] = []
        x_values: list[float] = []
        y_values: list[float] = []
        width_values: list[float] = []
        height_values: list[float] = []
        for idx, box in enumerate(bboxes):
            if box is not None:
                indices.append(idx)
                x_values.append(box['bounding_box']["x"] / (width / 2) - 1)
                y_values.append(box['bounding_box']["y"] / (height / 2) - 1)
                width_values.append(box['bounding_box']["width"] / (width / 2) - 1)
                height_values.append(box['bounding_box']["height"] / (height / 2) - 1)
        if len(indices) <= 2:
            return any(bounding_box_utils.has_intersections_generator(bbox, bboxes))

        if self.tracker_dict[tracker_key]["guess_next"] is None:
            fit_h, fit_w, fit_x, fit_y = calculate_fitting_parameters(height_values,
                                                                      indices,
                                                                      width_values,
                                                                      x_values, y_values)

            h_guess, w_guess, x_guess, y_guess = calculate_guess_bounding_box_values(fit_h,
                                                                                     fit_w,
                                                                                     fit_x,
                                                                                     fit_y,
                                                                                     height,
                                                                                     width)

            self.tracker_dict[tracker_key]["guess_params"] = my_types.GuessParams(fit_x=fit_x,
                                                                                  fit_y=fit_y,
                                                                                  fit_w=fit_w,
                                                                                  fit_h=fit_h)

            self.tracker_dict[tracker_key]["guess_next"] = my_types.BoundingBoxCenter(x=x_guess,
                                                                                      y=y_guess,
                                                                                      width=w_guess,
                                                                                      height=h_guess)
        else:
            x_guess = self.tracker_dict[tracker_key]["guess_next"]["x"]
            y_guess = self.tracker_dict[tracker_key]["guess_next"]["y"]
            w_guess = self.tracker_dict[tracker_key]["guess_next"]["width"]
            h_guess = self.tracker_dict[tracker_key]["guess_next"]["height"]

        return_value: bool = abs(bbox["x"] - x_guess) <= bbox["width"] / 2 \
                             and abs(bbox["y"] - y_guess) <= bbox["height"] / 4 \
                             and abs(bbox["width"] - w_guess) <= bbox["width"] / 2 \
                             and abs(bbox["height"] - h_guess) <= bbox["height"] / 4

        if len(indices) <= 5:
            return return_value and any(
                bounding_box_utils.has_intersections_generator(bbox, bboxes))
        return return_value

    def track(self, bbox_in: my_types.PersonBoundingBox, height: int, width: int) -> int:
        """
        track a specific bounding box
        :param bbox_in:
        :param height:
        :param width:
        :return:
        """
        bbox_center_coordinates = bounding_box_utils.convert_bbox_to_center_format(
            my_types.BoundingBoxEdges(left=bbox_in["left"], right=bbox_in["right"],
                                      top=bbox_in["top"],
                                      bottom=bbox_in["bottom"]))
        candidates: list[int] = []
        check_threads = []
        with lock:
            for tracker_key, tracker_val in self.tracker_dict.items():
                thread = threading.Thread(target=self.calculate_candidates, args=(
                    bbox_center_coordinates, candidates, height, tracker_key, tracker_val, width))
                check_threads.append(thread)
                thread.start()

            for thread in check_threads:
                thread.join()

        result: int
        if len(candidates) == 0:
            with lock:
                self.tracker_dict[self._tracker_index] = my_types.TrackingData(
                    id=self._tracker_index, bounding_boxes=[],
                    first_bounding_box=my_types.BoundingBoxTracking(
                        bounding_box=bbox_center_coordinates,
                        confidence=bbox_in["confidence"]),
                    guess_next=None,
                    guess_params=None, payload=None)
            result = self._tracker_index
            self._tracker_index += 1
        elif len(candidates) == 1:
            result = candidates[0]
        else:
            area_bbox = bbox_center_coordinates["height"] * bbox_center_coordinates["width"]
            intersection_bbox: dict[int, float] = {}
            intersection_candidate: dict[int, float] = {}
            for candidate in candidates:
                bbox_candidate: my_types.BoundingBoxTracking | None = None
                with lock:
                    for bbox in self.tracker_dict[candidate]["bounding_boxes"]:
                        if bbox is not None:
                            bbox_candidate = bbox
                            break
                candidate_bbox = bounding_box_utils.convert_center_format_to_bbox(
                    bbox_candidate["bounding_box"])
                area_candidate = bbox_candidate["bounding_box"]["height"] * \
                                 bbox_candidate["bounding_box"]["width"]
                left = bbox_in["left"] if bbox_in["left"] > candidate_bbox["left"] else \
                    candidate_bbox["left"]
                top = bbox_in["top"] if bbox_in["top"] > candidate_bbox["top"] else candidate_bbox[
                    "top"]
                right = bbox_in["right"] if bbox_in["right"] < candidate_bbox["right"] else \
                    candidate_bbox["right"]
                bottom = bbox_in["bottom"] if bbox_in["bottom"] < candidate_bbox["bottom"] else \
                    candidate_bbox["bottom"]

                area_intersection = (right - left) * (bottom - top)
                intersection_bbox[candidate] = area_intersection / area_bbox
                intersection_candidate[candidate] = area_intersection / area_candidate

            first_found_max_val_key = max(intersection_bbox, key=lambda x: intersection_bbox[x])
            max_val_keys = [k for k, v in intersection_bbox.items() if
                            v == intersection_bbox[first_found_max_val_key]]
            if len(max_val_keys) == 1:
                result = first_found_max_val_key
            else:
                result = max(max_val_keys, key=lambda x: intersection_candidate[x])

        self._last_seen_items.append(my_types.SeenItem(
            tracking_key=result, bounding_box=bbox_center_coordinates,
            confidence=bbox_in["confidence"]))

        return result

    def calculate_candidates(self, bbox_center_coordinates: my_types.BoundingBoxCenter,
                             candidates: list[int],
                             height: int, tracker_key: int, tracker_val: my_types.TrackingData,
                             width: int) -> None:
        """
        calculate all candidates for a given bounding box
        :param bbox_center_coordinates:
        :param candidates:
        :param height:
        :param tracker_key:
        :param tracker_val:
        :param width:
        """
        if self._is_candidate(bbox_center_coordinates, tracker_val["bounding_boxes"], tracker_key,
                              height, width):
            candidates.append(tracker_key)

    def check_if_moved(self, key: int) -> bool:
        """
        check if a specific tracking item has moved since it appeared
        :param key:
        :return:
        """
        bbox_first = self.tracker_dict[key]["first_bounding_box"]
        bbox_last = [bb for bb in self.tracker_dict[key]["bounding_boxes"] if bb is not None][0]
        distance_travelled = math.sqrt(
            (bbox_first['bounding_box']['x'] - bbox_last['bounding_box']['x']) ** 2 + (
                    bbox_first['bounding_box']['y'] - bbox_last['bounding_box']['y']) ** 2)
        height_growth = bbox_last['bounding_box']['height'] / bbox_first['bounding_box']['height']
        width_growth = bbox_last['bounding_box']['width'] / bbox_first['bounding_box']['width']
        return distance_travelled > bbox_first['bounding_box']['width'] or (
                height_growth < 0.5 and width_growth < 0.5) or (
                width_growth > 1.5 and height_growth > 1.5)

    async def update_tracker(self, websocket, source: str, print_data: bool) -> None:
        """
        update the tracker after all calculations
        :param print_data:
        :param websocket:
        :param source:
        """
        with lock:
            to_remove: list[int] = []
            to_discard: list[int] = []
            for key, value in self.tracker_dict.items():
                value["guess_next"] = None
                ids_last_seen_items = [last_seen_item["tracking_key"] for last_seen_item in
                                       self._last_seen_items]
                if ids_last_seen_items.count(key) != 1:
                    add_bounding_box(value, None, 0.0)
                    if len(value["bounding_boxes"]) >= MAX_LENGTH_OF_SAVED_BOUNDING_BOXES / 2 and \
                            value[
                                "bounding_boxes"].count(
                                None) >= MAX_LENGTH_OF_SAVED_BOUNDING_BOXES * 0.9:
                        if self.check_if_moved(key) or self.tracker_dict[key]['payload'][
                            'timestamp_detection'] is not None:
                            to_remove.append(key)
                        else:
                            to_discard.append(key)
                    elif len(
                            value["bounding_boxes"]) < MAX_LENGTH_OF_SAVED_BOUNDING_BOXES / 2 and \
                            value[
                                "bounding_boxes"].count(
                                None) >= MAX_LENGTH_OF_SAVED_BOUNDING_BOXES * 0.325:
                        if self.tracker_dict[key]['payload']['timestamp_detection'] is None:
                            to_discard.append(key)
                if ids_last_seen_items.count(key) > 1:
                    indexes_last_seen_items = [idx for idx, item in enumerate(self._last_seen_items)
                                               if
                                               item["tracking_key"] == key]
                    index_correcture = 0
                    for index in indexes_last_seen_items:
                        item = self._last_seen_items.pop(index - index_correcture)
                        index_correcture += 1
            for last_seen_item in self._last_seen_items:
                add_bounding_box(self.tracker_dict[last_seen_item["tracking_key"]],
                                 last_seen_item["bounding_box"],
                                 last_seen_item["confidence"])
            for key in to_discard:
                self.tracker_dict.pop(key)
            if len(to_remove) > 0:
                for key in to_remove:
                    item = self.tracker_dict.pop(key)
                    await process_closed_tracking_data(item, source, websocket)
                    await asyncio.sleep(0)
                    if len(item['payload']['bibs']) > 0:
                        bibs = [{"text": x["text"],
                                 "rating": person_tracking_payload_utils.calc_bib_rating(
                                     x["confidence"], x["seen"]),
                                 "confidence": x["confidence"], "seen": x["seen"]} for x in
                                item['payload']['bibs']]
                        bibs.sort(key=lambda x: x['rating'], reverse=True)
                        if print_data:
                            print(
                                f"Time: {item['payload']['timestamp_best_image']}, Bibs: {bibs[0:3]}")
            self._last_seen_items = []

    def set_payload(self, tracking_id: int, payload: my_types.PersonTrackingPayload) -> None:
        """
        setter for payload
        :param tracking_id:
        :param payload:
        """
        self.tracker_dict[tracking_id]["payload"] = payload

    def get_payload(self, tracking_id: int) -> my_types.PersonTrackingPayload | None:
        """
        getter for payload
        :param tracking_id:
        :return:
        """
        return self.tracker_dict[tracking_id]["payload"]
