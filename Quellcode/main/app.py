"""
app for the track camera
"""
import argparse
import asyncio
import multiprocessing.connection
import os
import sys
import time

import cv2

import bounding_box_utils
import my_types
from common_app_utils import tracking_and_matching
from object_detection import bib_detection, person_detection
from person_tracking.tracker import ObjectTracker

parser = argparse.ArgumentParser(description="Read video to process for identification")
parser.add_argument("-i", "--input", type=str, help="Path to the video file")
args = parser.parse_args()

if not args.input:
    print("No input paramater have been given.")
    print("For help type --help")
    sys.exit()

if os.path.splitext(args.input)[1] != ".mp4" and os.path.splitext(args.input)[1] != ".MP4":
    print("The given file is not of correct file format.")
    print("Only .mp4 files are accepted")
    sys.exit()

VIDEO_FILE = args.input

person_tracker = ObjectTracker()

bib_detection_connection: multiprocessing.connection.Connection
tracking_matching_connection: multiprocessing.connection.Connection
person_detection_connection_YOLO: multiprocessing.connection.Connection


async def main():
    """
    the main program
    """
    start = time.time()
    # Open file and read props
    cap = cv2.VideoCapture(VIDEO_FILE)
    frames_to_play = cap.get(cv2.CAP_PROP_FRAME_COUNT)
    fps = cap.get(cv2.CAP_PROP_FPS)
    if not cap.isOpened():
        # print("Cannot open file")
        sys.exit()

    # main loop
    while True:
        current_frame = cap.get(cv2.CAP_PROP_POS_FRAMES)
        if current_frame >= frames_to_play - 1:
            break
        # Capture frame-by-frame
        ret, frame = cap.read()
        # if frame is read correctly ret is True
        if not ret or current_frame % 2 != 0:
            continue

        timestamp = cap.get(cv2.CAP_PROP_POS_FRAMES) / fps

        frame_out = cv2.resize(frame, (int(frame.shape[1] / 2), int(frame.shape[0] / 2)))

        # -------------------------------- detect bibs and persons --------------------------------

        bib_detection_connection.send(frame.copy())
        person_detection_connection_YOLO.send(frame.copy())

        # ----------------------------------- do person tracking -----------------------------------

        if not bib_detection_connection.poll(1):
            continue
        bibs: list[my_types.Detection] = bib_detection_connection.recv()
        if not person_detection_connection_YOLO.poll(1):
            continue
        persons: list[my_types.DetectionYOLO] = person_detection_connection_YOLO.recv()
        person_bounding_boxes: list[my_types.PersonBoundingBox] = []
        for bbox_in in persons:
            bbox = bounding_box_utils.convert_center_format_to_bbox(bbox_in['bounding_box'])
            person_bounding_boxes.append(
                my_types.PersonBoundingBox(confidence=bbox_in['confidence'] / 100,
                                           bottom=bbox['bottom'], top=bbox['top'],
                                           right=bbox['right'], left=bbox['left'], time=None))
            cv2.rectangle(frame_out, (int(bbox['left'] / 2), int(bbox['top'] / 2)),
                          (int(bbox['right'] / 2), int(bbox['bottom'] / 2)), (255, 0, 0), 1)
        tracking_matching_connection.send(
            (bibs, frame, person_bounding_boxes, VIDEO_FILE, timestamp))

        # ------------------------------------- postprocessing -------------------------------------

        run_time = time.time() - start
        start = time.time()
        cv2.putText(frame_out, "FPS: " + str(int(1 / run_time)), (8, 30),
                    fontFace=cv2.FONT_HERSHEY_PLAIN,
                    color=(0, 0, 255), fontScale=2, thickness=3)
        cv2.imshow('frame', frame_out)
        if cv2.waitKey(1) == ord('q'):
            break
    # When everything done, release the capture
    cap.release()
    cv2.destroyAllWindows()


if __name__ == "__main__":
    bib_detection_connection, bib_detection_connection_2 = multiprocessing.Pipe(duplex=True)
    person_detection_connection_YOLO, person_detection_connection_YOLO_2 = multiprocessing.Pipe(
        duplex=True)
    tracking_matching_connection, tracking_matching_connection_2 = multiprocessing.Pipe()
    bib_process = multiprocessing.Process(target=bib_detection.detect_bibs,
                                          args=(0.5, bib_detection_connection_2))
    bib_process.start()
    person_process = multiprocessing.Process(target=person_detection.detect_persons,
                                             args=(0.5, person_detection_connection_YOLO_2))
    person_process.start()
    tracking_matching_process = multiprocessing.Process(target=tracking_and_matching,
                                                        args=(tracking_matching_connection_2,
                                                              person_tracker, True))
    tracking_matching_process.start()
    asyncio.run(main())
    bib_process.join()
    person_process.join()
    tracking_matching_process.join()
