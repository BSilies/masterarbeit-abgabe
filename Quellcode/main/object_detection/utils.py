"""
some commonly used functions for object detection
"""
import cv2

from darknet import darknet


async def detect_objects(image, class_names, network, thresh):
    """
    detect object from a given image using a given network
    :param image:
    :param class_names:
    :param network:
    :param thresh:
    :return:
    """
    # --------------------------------------- prepare image ---------------------------------------
    width = darknet.network_width(network)
    height = darknet.network_height(network)
    image_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    image_resized = cv2.resize(image_rgb, (width, height), interpolation=cv2.INTER_LINEAR)
    height_in, width_in, _ = image.shape
    height_resized, width_resized, _ = image_resized.shape
    scale_x = width_in / width_resized
    scale_y = height_in / height_resized
    darknet_image = darknet.make_image(width, height, 3)
    darknet.copy_image_from_bytes(darknet_image, image_resized.tobytes())
    # ---------------------------------------- do detection ----------------------------------------
    detections = darknet.detect_image(network, class_names, darknet_image, thresh=thresh)
    darknet.free_image(darknet_image)
    return detections, scale_x, scale_y
