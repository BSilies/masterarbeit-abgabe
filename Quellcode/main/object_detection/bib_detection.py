"""
module for bib detection and identification
"""
import asyncio
import logging
import multiprocessing.connection
import pickle

import numpy as np
import websockets

import my_types
import object_detection.utils
from bounding_box_utils import convert_center_format_to_bbox
from darknet import darknet


def detect_bibs(thresh: float, connection: multiprocessing.connection.Connection):
    """
    entrypoint for bib detection
    :param thresh:
    :param connection:
    """
    asyncio.run(run(thresh, connection))


async def run(thresh: float, connection: multiprocessing.connection.Connection):
    """
    forever runnning function for bib detection
    :param thresh:
    :param connection:
    """
    uri = "ws://localhost:8004"
    network, class_names, _ = darknet.load_network(
        "C:/Users/Bastian/Documents/Studium/Master_Thesis/master-thesis/Code/FinalApplication/main/darknet/network/bib_detection.cfg",
        "C:/Users/Bastian/Documents/Studium/Master_Thesis/master-thesis/Code/FinalApplication/main/darknet/network/bib_detection.data",
        "C:/Users/Bastian/Documents/Studium/Master_Thesis/master-thesis/Code/FinalApplication/main/darknet/network/bib_detection.weights",
    )

    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger()

    async with websockets.connect(uri, logger=logger, max_size=2 ** 22) as websocket:
        while True:
            if not connection.poll(10):
                break
            await asyncio.sleep(0)

            image: np.ndarray
            image = connection.recv()

            detections, scale_x, scale_y = await object_detection.utils.detect_objects(image,
                                                                                       class_names,
                                                                                       network,
                                                                                       thresh)
            # --------------------------------- process detections ---------------------------------

            result: list[my_types.Detection] = []
            for detection in detections:

                # -------------------------------- prepare for ocr --------------------------------

                bounding_box_center = my_types.BoundingBoxCenter(x=detection[2][0] * scale_x,
                                                                y=detection[2][1] * scale_y,
                                                                width=detection[2][2] * scale_x,
                                                                height=detection[2][3] * scale_y)

                bounding_box = convert_center_format_to_bbox(bounding_box_center)
                bib_image: np.array = image[int(bounding_box["top"]):int(bounding_box["bottom"]),
                                      int(bounding_box["left"]):int(bounding_box["right"])].copy()

                # ------------------------------------- do ocr -------------------------------------

                await websocket.send(pickle.dumps(bib_image))
                identification_in: tuple[list[list[float]], str, float] = pickle.loads(
                    await websocket.recv())
                await asyncio.sleep(0)

                # --------------------------------- produce result ---------------------------------

                identification: my_types.BibDetection | None = None
                if identification_in is not None:
                    identification = my_types.BibDetection(
                        bbox=[my_types.Point(x=x[0], y=x[1]) for x in identification_in[0]],
                        text=identification_in[1], confidence=identification_in[2])
                result.append(my_types.Detection(confidence=float(detection[1]) / 100,
                                                bounding_box=bounding_box_center,
                                                bib=identification))
            connection.send(result)
