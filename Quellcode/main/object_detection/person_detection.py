"""
module for person detection
"""
import asyncio
import multiprocessing.connection

import numpy as np

import my_types
import object_detection.utils
from darknet import darknet


def detect_persons(thresh: float, connection: multiprocessing.connection.Connection):
    """
    entrypoint for person detection
    :param thresh:
    :param connection:
    """
    asyncio.run(run(thresh, connection))


async def run(thresh: float, connection: multiprocessing.connection.Connection):
    """
    forever runnning function for person detection
    :param thresh:
    :param connection:
    """
    network, class_names, _ = darknet.load_network(
        "C:/Users/Bastian/Documents/Studium/Master_Thesis/master-thesis/Code/FinalApplication/main/darknet/network/coco.cfg",
        "C:/Users/Bastian/Documents/Studium/Master_Thesis/master-thesis/Code/FinalApplication/main/darknet/network/coco.data",
        "C:/Users/Bastian/Documents/Studium/Master_Thesis/master-thesis/Code/FinalApplication/main/darknet/network/coco.weights",
    )

    while True:
        if not connection.poll(10):
            break
        await asyncio.sleep(0)

        image: np.ndarray
        image = connection.recv()

        detections, scale_x, scale_y = await object_detection.utils.detect_objects(image,
                                                                                   class_names,
                                                                                   network, thresh)

        # ------------------------------------- produce result -------------------------------------

        result: list[my_types.DetectionYOLO] = []
        for detection in [d for d in detections if d[0] == "person"]:
            bounding_box_center = my_types.BoundingBoxCenter(x=detection[2][0] * scale_x,
                                                            y=detection[2][1] * scale_y,
                                                            width=detection[2][2] * scale_x,
                                                            height=detection[2][3] * scale_y)

            result.append(my_types.DetectionYOLO(confidence=float(detection[1]) / 100,
                                                bounding_box=bounding_box_center))
        connection.send(result)
