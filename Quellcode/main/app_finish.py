"""
app for the finish camera
"""
import argparse
import asyncio
import csv
import datetime
import multiprocessing.connection
import os
import queue
import sys
import time

import cv2

import bounding_box_utils
import finish_utils
import my_types
from common_app_utils import tracking_and_matching
from finish_utils import calculate_head_points_for_detections, match_detections_to_persons, \
    is_head_point_in_bounding_box
from object_detection import bib_detection, person_detection
from person_tracking.tracker import ObjectTracker

parser = argparse.ArgumentParser(description="Read video to process for identification")
parser.add_argument("-i", "--input", type=str, help="Path to the video file")
parser.add_argument("-d", "--detections", type=str, help="Path to the detections file")
args = parser.parse_args()

if not args.input:
    print("No input paramater have been given.")
    print("For help type --help")
    sys.exit()

if os.path.splitext(args.input)[1] != ".mp4" and os.path.splitext(args.input)[1] != ".MP4":
    print("The given file is not of correct file format.")
    print("Only .mp4 files are accepted")
    sys.exit()

if not args.detections:
    print("No detections paramater have been given.")
    print("For help type --help")
    sys.exit()

if os.path.splitext(args.detections)[1] != ".csv":
    print("The given file is not of correct file format.")
    print("Only .csv files are accepted")
    sys.exit()

VIDEO_FILE = args.input

person_tracker = ObjectTracker()

bib_detection_connection: multiprocessing.connection.Connection
tracking_matching_connection: multiprocessing.connection.Connection
person_detection_connection: multiprocessing.connection.Connection


#      A                                          BAC  = arccos((b^2+c^2-a^2)/2bc)
#      | \^                                       CBA  = arccos((a^2+c^2-b^2)/2ac)
#       |   \ ^                                   b1   = sqrt(x^2+c^2-2xc*cos(CBA)
#        |     \  ^                               C2BA = 90°-BAC/2 = AC2B
#         |       \   ^                           BAC1 = arcsin((x*sin(C2BA))/b1)
#          |         \    ^                       a1   = arcsin((c*sin(BAC1))/sin(AC2B))
#           |           \     ^                   x1   = arcsin((c*sin(BAC1))/sin(180°-(BAC1+C2BA)))
#            |             \b1    ^
#           c |               \       ^  b
#              |                 \        ^
#               |                   \         ^ C2
#                |                     \ __--²²   ^
#                 |         x1    __--²²  \    <-a1   ^
#                  |       __--²²            \            ^
#                   |--_________________________\_____________^
#                    B           x              C1     <-a     C


async def main():
    """
    the main program
    """
    my_queue = queue.Queue()

    with open(args.detections, newline='', encoding="utf_8") as file:
        filereader = csv.reader(file, delimiter=';', quotechar='"')
        for row in filereader:
            my_queue.put(
                (datetime.datetime.fromtimestamp(float(row[0])), float(row[1]), float(row[2])))

    start = time.time()
    cap = cv2.VideoCapture(VIDEO_FILE)
    frames_to_play = cap.get(cv2.CAP_PROP_FRAME_COUNT)
    fps = cap.get(cv2.CAP_PROP_FPS)
    if not cap.isOpened():
        # print("Cannot open file")
        sys.exit()

    next_detection: tuple[datetime.datetime, float, float] = my_queue.get()
    last_time = finish_utils.START_TIME

    head_points: list[list[datetime.datetime | tuple[float, float] | list[int] | int]] = []

    while True:
        current_frame = cap.get(cv2.CAP_PROP_POS_FRAMES)
        if current_frame >= frames_to_play - 1:
            break
        # Capture frame-by-frame
        ret, frame_in = cap.read()
        # if frame is read correctly ret is True
        if not ret or current_frame % 2 != 0:
            continue

        play_time = finish_utils.START_TIME + datetime.timedelta(seconds=current_frame / fps)

        frame = cv2.resize(frame_in, (
            int(frame_in.shape[1] * finish_utils.FACTOR_WORK),
            int(frame_in.shape[0] * finish_utils.FACTOR_WORK)))
        frame_out = cv2.resize(frame_in, (
            int(frame_in.shape[1] * finish_utils.FACTOR_SHOW),
            int(frame_in.shape[0] * finish_utils.FACTOR_SHOW)))
        frame_out = cv2.copyMakeBorder(frame_out, 0, 100, 0, 0, cv2.BORDER_CONSTANT,
                                       value=(255, 255, 255))

        # -------------------------------- detect bibs and persons --------------------------------

        bib_detection_connection.send(frame.copy())
        person_detection_connection.send(frame.copy())

        # ----------------------------------- do person tracking -----------------------------------

        if not bib_detection_connection.poll(1):
            continue
        bibs: list[my_types.Detection] = bib_detection_connection.recv()
        if not person_detection_connection.poll(1):
            continue
        persons: list[my_types.DetectionYOLO] = person_detection_connection.recv()
        person_bounding_boxes: list[my_types.PersonBoundingBox] = []

        next_detection = await calculate_head_points_for_detections(frame_out, head_points,
                                                                    last_time, my_queue,
                                                                    next_detection, play_time)
        for idx, bbox_in in enumerate(persons):
            bbox = bounding_box_utils.convert_center_format_to_bbox(bbox_in['bounding_box'])
            person_bounding_boxes.append(
                my_types.PersonBoundingBox(confidence=bbox_in['confidence'] / 100,
                                           bottom=bbox['bottom'], top=bbox['top'],
                                           right=bbox['right'], left=bbox['left'], time=None))
            for head in head_points:
                if is_head_point_in_bounding_box(bbox, bbox_in, head):
                    head[2].append(idx)
            cv2.rectangle(frame_out, (
                int(bbox['left'] * finish_utils.FACTOR_WORK_TO_SHOW),
                int(bbox['top'] * finish_utils.FACTOR_WORK_TO_SHOW)),
                          (int(bbox['right'] * finish_utils.FACTOR_WORK_TO_SHOW),
                           int(bbox['bottom'] * finish_utils.FACTOR_WORK_TO_SHOW)),
                          (255, 0, 0), 1)

        head_to_remove = []

        await match_detections_to_persons(fps, frame_out, head_points, head_to_remove,
                                          person_bounding_boxes)
        index_correcture = 0
        for idx in head_to_remove:
            head_points.pop(idx - index_correcture)
            index_correcture += 1
        tracking_matching_connection.send(
            (bibs, frame, person_bounding_boxes, VIDEO_FILE, play_time.time()))
        run_time = time.time() - start
        start = time.time()
        cv2.putText(frame_out, F"FPS: {int(1 / run_time)}", (8, 30),
                    fontFace=cv2.FONT_HERSHEY_PLAIN,
                    color=(0, 0, 255), fontScale=2, thickness=3)
        cv2.putText(frame_out,
                    play_time.isoformat(timespec='milliseconds') + str(int(1 / run_time)),
                    (8, frame_out.shape[0] - 8),
                    fontFace=cv2.FONT_HERSHEY_PLAIN,
                    color=(0, 0, 255), fontScale=2, thickness=3)

        cv2.circle(frame_out,
                   (int(finish_utils.REF_POINTS[0][0][0] * finish_utils.FACTOR_SHOW),
                    int(finish_utils.REF_POINTS[0][0][1] * finish_utils.FACTOR_SHOW)),
                   2,
                   (0, 0, 255), -1)
        cv2.circle(frame_out,
                   (int(finish_utils.REF_POINTS[0][1][0] * finish_utils.FACTOR_SHOW),
                    int(finish_utils.REF_POINTS[0][1][1] * finish_utils.FACTOR_SHOW)),
                   2,
                   (0, 0, 255), -1)
        cv2.circle(frame_out,
                   (int(finish_utils.REF_POINTS[1][0][0] * finish_utils.FACTOR_SHOW),
                    int(finish_utils.REF_POINTS[1][0][1] * finish_utils.FACTOR_SHOW)),
                   2,
                   (0, 0, 255), -1)
        cv2.circle(frame_out,
                   (int(finish_utils.REF_POINTS[1][1][0] * finish_utils.FACTOR_SHOW),
                    int(finish_utils.REF_POINTS[1][1][1] * finish_utils.FACTOR_SHOW)),
                   2,
                   (0, 0, 255), -1)
        cv2.imshow('frame', frame_out)
        if cv2.waitKey(1) == ord('q'):
            break
        last_time = play_time
    # When everything done, release the capture
    cap.release()
    cv2.destroyAllWindows()


if __name__ == "__main__":
    bib_detection_connection, bib_detection_connection_2 = multiprocessing.Pipe(duplex=True)
    person_detection_connection, person_detection_connection_2 = multiprocessing.Pipe(
        duplex=True)
    tracking_matching_connection, tracking_matching_connection_2 = multiprocessing.Pipe()
    bib_process = multiprocessing.Process(target=bib_detection.detect_bibs,
                                          args=(0.5, bib_detection_connection_2))
    bib_process.start()
    person_process = multiprocessing.Process(
        target=person_detection.detect_persons,
        args=(0.5, person_detection_connection_2))
    person_process.start()
    tracking_matching_process = multiprocessing.Process(target=tracking_and_matching,
                                                        args=(tracking_matching_connection_2,
                                                              person_tracker, False))
    tracking_matching_process.start()
    asyncio.run(main())
    bib_process.join()
    person_process.join()
    tracking_matching_process.join()
