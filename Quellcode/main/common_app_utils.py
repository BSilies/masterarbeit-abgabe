import asyncio
import logging
import multiprocessing.connection
import threading

import websockets

import my_types
from person_bib_matching.do_matching import match_bib_to_person, update_lonely_bibs
from person_tracking.do_tracking import track_person


def tracking_and_matching(connection: multiprocessing.connection.Connection, person_tracker,
                          print_data):
    """
    entrypoint for tracking and matching process
    :param person_tracker:
    :param connection:
    """
    asyncio.run(tracking_and_matching_run(connection, person_tracker, print_data))


async def tracking_and_matching_run(connection: multiprocessing.connection.Connection,
                                    person_tracker, print_data: bool):
    """
    the tracking and matching process
    :param person_tracker:
    :param connection:
    """
    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger()
    uri = "ws://localhost:8005"

    async with websockets.connect(uri, logger=logger, max_size=2 ** 28) as websocket:
        while True:
            await asyncio.sleep(0)
            if not connection.poll(5):
                break

            person_bounding_boxes: list[my_types.PersonBoundingBox]
            bibs: list[my_types.Detection]
            bibs, frame, person_bounding_boxes, source, timestamp = connection.recv()
            height, width, _ = frame.shape
            tracking_ids = []
            tracking_threads = []

            # ------------------------------------ do tracking ------------------------------------

            for person_bounding_box in person_bounding_boxes:
                thread = threading.Thread(target=track_person,
                                          args=(
                                              frame.copy(), height, person_bounding_box,
                                              tracking_ids,
                                              width,
                                              person_tracker, timestamp))
                tracking_threads.append(thread)
                thread.start()
            for thread in tracking_threads:
                thread.join()
            await person_tracker.update_tracker(websocket, source, print_data)

            # -------------------------------- match bibs to person --------------------------------
            relevant_bibs = [x for x in bibs if x['bib'] is not None]
            matching_threads = []
            for bib in relevant_bibs:
                thread = threading.Thread(target=match_bib_to_person,
                                          args=(bib, frame.copy(), person_tracker))
                matching_threads.append(thread)
                thread.start()
            for thread in matching_threads:
                thread.join()
            update_lonely_bibs(relevant_bibs)
