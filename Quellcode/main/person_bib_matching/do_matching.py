"""
everything related to matching bibs and persons
"""
import threading

import numpy as np

import my_types
from bounding_box_utils import has_intersection, convert_center_format_to_bbox
from person_tracking import tracker
from person_tracking.person_tracking_payload_utils import add_bib, \
    MAX_LENGTH_OF_SAVED_BOUNDING_BOXES

lock = threading.Lock()
lock2 = threading.Lock()

lonely_bibs: list[my_types.LonelyBib] = []


def match_bib_to_person(detected_bib: my_types.Detection, frame: np.ndarray,
                        person_tracker: tracker.ObjectTracker):
    """
    find fitting entrie in tracking for detected bib
    :param detected_bib:
    :param frame:
    :param person_tracker:
    """
    candidates = []

    get_candidates(candidates, detected_bib, person_tracker)

    if len(candidates) > 0:

        with lock:
            lonely_bibs_pop_correcture = 0
            for idx in [idx for idx, value in enumerate(lonely_bibs) if
                        value["bib"]["bib"]["text"] == detected_bib["bib"]["text"]]:
                lonely_bibs.pop(idx - lonely_bibs_pop_correcture)
                lonely_bibs_pop_correcture += 1
        candidates.sort(key=lambda x: (x[1][1], -x[1][2]), reverse=True)
        candidate = candidates[0]
        payload = person_tracker.get_payload(candidate[0])
        if payload is None:
            payload = my_types.PersonTrackingPayload(bibs=[], person_confidence=None,
                                                     best_image_person=None,
                                                     best_image_bib=None, timestamp_best_image=0,
                                                     timestamp_detection=None,
                                                     detection_image_person=None)
            person_tracker.set_payload(candidate[0], payload)
        update_payload = add_bib(payload, my_types.Bib(text=detected_bib["bib"]["text"],
                                                       confidence=calculate_confidence(
                                                           detected_bib)))

        if update_payload:
            if not candidate[3] and candidate[2]["width"] > 0 and candidate[2]["height"] > 0:
                bbox = convert_center_format_to_bbox(candidate[2])
                person_image: np.array = frame[int(bbox["top"]):int(bbox["bottom"]),
                                         int(bbox["left"]):int(bbox["right"])].copy()
                payload["best_image_bib"] = person_image

    else:
        with lock2:
            indices_of_current_bib_in_lonely_bibs = [idx for idx, value in enumerate(lonely_bibs) if
                                                     value["bib"]['bib']["text"] ==
                                                     detected_bib["bib"]["text"]]
            if len(indices_of_current_bib_in_lonely_bibs) > 0 and \
                    lonely_bibs[indices_of_current_bib_in_lonely_bibs[0]]["bib"]["bib"][
                        "confidence"] < detected_bib["bib"]["confidence"]:
                lonely_bibs[indices_of_current_bib_in_lonely_bibs[0]] = \
                    {"bib": detected_bib,
                     "seen": lonely_bibs[indices_of_current_bib_in_lonely_bibs[0]]["seen"] + 1,
                     "last_seen": 0}
            elif len(indices_of_current_bib_in_lonely_bibs) > 0:
                lonely_bibs[indices_of_current_bib_in_lonely_bibs[0]]["seen"] += 1
                lonely_bibs[indices_of_current_bib_in_lonely_bibs[0]]["last_seen"] = 0
            elif len(indices_of_current_bib_in_lonely_bibs) == 0:
                lonely_bibs.append(my_types.LonelyBib(bib=detected_bib, seen=1, last_seen=0))


def calculate_confidence(detected_bib: my_types.Detection) -> float:
    """
    calculates the combined confidence as weighted mean from detection confidence and ocr confidence
    :param detected_bib:
    :return: combined confidence
    """
    return 2 * detected_bib["bib"]["confidence"] * detected_bib["confidence"] / \
        (detected_bib["bib"]["confidence"] + detected_bib["confidence"])


def get_candidates(candidates, detected_bib, person_tracker):
    """
    get all possible candidates from tracking for one bib
    :param candidates:
    :param detected_bib:
    :param person_tracker:
    """
    for tracker_key, tracker_value in person_tracker.tracker_dict.items():
        is_guess = True
        bounding_box: my_types.BoundingBoxCenter
        if tracker_value["bounding_boxes"][0] is not None:
            intersection = has_intersection(detected_bib["bounding_box"],
                                            tracker_value["bounding_boxes"][0]["bounding_box"])
            bounding_box = tracker_value["bounding_boxes"][0]["bounding_box"]
            is_guess = False
        elif tracker_value["guess_next"] is not None:
            intersection = has_intersection(detected_bib["bounding_box"],
                                            tracker_value["guess_next"])
            bounding_box = tracker_value["guess_next"]
        else:
            intersection = bool(False), 0.0, 0.0
        if intersection[0]:
            candidates.append((tracker_key, intersection, bounding_box, is_guess))


def update_lonely_bibs(relevant_bibs: list[my_types.Detection]):
    """
    save seen bibs that could not be matched
    :param relevant_bibs:
    """
    relevant_bibs_detection = [x["bib"]["text"] for x in relevant_bibs]
    lonely_bibs_to_remove: list[int] = []
    for index, lonely_bib in enumerate(lonely_bibs):
        if relevant_bibs_detection.count(lonely_bib["bib"]["bib"]["text"]) == 0:
            lonely_bib["last_seen"] += 1
        else:
            lonely_bib["seen"] += 1
        if lonely_bib["last_seen"] > MAX_LENGTH_OF_SAVED_BOUNDING_BOXES:
            lonely_bibs_to_remove.append(index)
    index_correction = 0
    for index in lonely_bibs_to_remove:
        lonely_bibs.pop(index - index_correction)
        index_correction += 1
