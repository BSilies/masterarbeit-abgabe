# Haupt-/Kameramodul zur Identifikation und Ergebnisermittlung

## Installation

Es wird Python Version 3.10 und pip benötigt.

Zusätzlich
muss [CUDA 11.7](https://developer.nvidia.com/cuda-11-7-0-download-archive?target_os=Windows&target_arch=x86_64&target_version=11&target_type=exe_local)
mit [CUDNN](https://docs.nvidia.com/deeplearning/cudnn/install-guide/index.html) installiert werden.
Eine NVIDIA-Grafikkarte mit CUDA-Unterstützung ist notwendig.

Je nach Betriebssystems das Script ``setup.bat`` oder ``setup.sh`` ausführen. In der
Datei ``setup.bat`` muss der Dateipfad zur Python-Installation angegeben werden.

## Ausführung

In jedem Fall müssen ``bibOCR`` und ``reidentificator`` gestartet sein.

### Streckenkamera - Identifikation

Die Anwendung kann nach der Installation in der virtuellen Umgebung mit dem
Befehl ``python app.py -i <<file-path>>`` gestartet werden. Es muss der Pfad
zu einer ``.mp4``-Datei angegeben werden, die ausgewertet werden soll. Die Identifikationsergebnisse
werden in der Konsole ausgegeben.

### Zielkamera - Ergebnisermittlung

Vor Ausführung des Programms sollte der Reidentifikations-Server neu gestartet werden und mittels
des Skripts ``python scripts\prepare_reidentification.py`` mit Reidentifikationsdaten befüllt
werden.

Die Anwendung kann nach der Installation in der virtuellen Umgebung mit dem
Befehl ``python app_finish.py -i <<file-path>> -d <<file-path>>`` gestartet werden. Es muss
für ``-i`` der Pfad zu einer ``.mp4``-Datei angegeben werden und für ``-p`` der Pfad zu
einer ``.csv``-Datei, die Detektionsdaten enthält. Diese stammt aus der ``athleteDetection``.
Unter ``./generatedData`` ist bereits die passende Datei für die mitgelieferten Daten abgelegt.
Die Gesamtergebnisse werden in der Konsole ausgegeben.

### Weitere Auswertungen

Die Reidentifikation kann mittels des Skripts ``python scripts\reidentification_test_images.py``
untersucht werden. Auch hierzu ist vorher der Server neu zu starten und das
Skript ``python scripts\prepare_reidentification.py`` auszuführen.

Der Test öffnet für jede Anfrage ein Fenster mit dem Ursprungsbild und erkannten Verwandtschaften.
ACHTUNG: Das geöffnete Fenster blockiert die Verbindung mit dem Server, diese wird nach einiger Zeit
automatisch geschlossen. Es empfiehlt sich daher die Betrachtungsdauer kurz zu halten.
